# bigdatateam-python-course-qa

Repository for Q&A at BigData Team python course:
- https://github.com/big-data-team/python-course

## How to submit a request

1. see other relevant issues (especially closed ones), which can help you to investigate your issue. Upvote similar ones and provide similar cases in the comments.
- https://gitlab.com/aadral/bigdatateam-python-course-qa/-/issues
- https://gitlab.com/aadral/bigdatateam-python-course-qa/-/boards

2. if there is nothing relevant to your case, create your own with the following schema

title: descriptive title
description:
- Task: the one you see in the Inputs when you submit the solution at the Grader, examples are python.asset_mock, python.inverted_index;
- Grader Link: (example) https://everest.distcomp.org/jobs/6196bfcb2f000068329917c4
- Investigation: provide information about your personal investigation, e.g. which message was not clear from the report (if it is available)? what are the specifics of your solution? any relevant information about your solution approach (without spoilers) which can be helpful for other learners and mentors.

Tag your issue with the label named by the Task. It is a duplicate for the Task in the description, but it would be likely more convinient to search and categorize items.

- https://gitlab.com/aadral/bigdatateam-python-course-qa/-/issues/new


## How to submit a feature request

see (How to submit a request), but use the following schema:
- title: the Grader should do/provide *** as it helps with ***
- description: pros/cons/reasoning
